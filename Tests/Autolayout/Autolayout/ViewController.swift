//
//  ViewController.swift
//  Autolayout
//
//  Created by Wendy Yang on 04/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    // iPhone landscape only - allow scrolling for bottomTextField
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomTextField: UITextField!
    
    @IBAction func submitButton(sender: UIButton) {
        self.view.endEditing(true) // retires keyboard when user touches submit button
    }
    
    @IBAction func hideKeyboard(sender: UITapGestureRecognizer) {
        self.view.endEditing(true) // retires keyboard when user toucher away from textField
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Scrolling function for bottomTextField when in iPhone landscape
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) {
            if textField == bottomTextField {
                scrollView.setContentOffset(CGPointMake(0, 43), animated: true)
            }
        }
    }
    
    // Retires any textField when return button is touched
    func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    
    return true
    }
    
    // Scroll back down when keyboard retire
    func textFieldDidEndEditing(textField: UITextField) {
        scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
    }

}

