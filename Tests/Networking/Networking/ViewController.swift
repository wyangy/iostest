//
//  ViewController.swift
//  Networking
//
//  Created by Wendy Yang on 04/02/2016.
//  Copyright © 2016 Wendy Yang. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

let managedContext = appDelegate.managedObjectContext

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var fetchedResultsController: NSFetchedResultsController!

    @IBOutlet var tableview: UITableView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBAction func segmentedControl(sender: UISegmentedControl) {
        tableview.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        importJsonDataIfNeeded()
        
        let fetchRequest = NSFetchRequest(entityName: "ToDo")
        
        let nameSort = NSSortDescriptor(key: "task", ascending: true)
        
        fetchRequest.sortDescriptors = [nameSort]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: appDelegate.managedObjectContext, sectionNameKeyPath: "task", cacheName: "networking")
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    // MARK: UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return fetchedResultsController.sections!.count
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionInfo = fetchedResultsController.sections![section]

        return sectionInfo.numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        let toDo = fetchedResultsController.objectAtIndexPath(indexPath) as! ToDo
        
        // Image
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = UIImage(named:"image.jpg")
        
        // Text label for "task"
        let textLabelForTask = cell.viewWithTag(2) as! UILabel
        textLabelForTask.text = toDo.task
        
        // Checkmark for "done"
        if toDo.done == true {
            cell.accessoryType = .Checkmark
        }
        else {
            cell.accessoryType = .None
        }
        
        return cell
    }
    
    // check is json has already been imported
    func importJsonDataIfNeeded() {
        
        let fetchRequest = NSFetchRequest(entityName: "ToDo")
        var error:NSError? = nil
        
        let count = appDelegate.managedObjectContext
            .countForFetchRequest(fetchRequest, error: &error)
        
        if count == 0 {
            importJsonData()
        }
    }
    
    // load data from json then save into core data
    func importJsonData() {
        let jsonURL = NSURL(string: "https://bitbucket.org/fatunicorn/iostest/raw/78f902ddb31ac96bb23b901054442dee52664271/data/tasklist.json")
        let jsonData = NSData(contentsOfURL: jsonURL!)
        
        do {
            let jsonArray = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: .AllowFragments) as! NSArray
            
            let entity =  NSEntityDescription.entityForName("ToDo", inManagedObjectContext: appDelegate.managedObjectContext)
            
            for jsonDictionary in jsonArray {
                let task = jsonDictionary["task"] as! String
                let done = jsonDictionary["done"] as! Bool
                let image = jsonDictionary["image"] as! String
                
                let toDo = ToDo(entity: entity!, insertIntoManagedObjectContext: appDelegate.managedObjectContext)
                toDo.task = task
                toDo.done = done
                toDo.image = image
            }
            
            appDelegate.saveContext()
        } catch let error as NSError  {
            print(error)
        }
    }
    
}
